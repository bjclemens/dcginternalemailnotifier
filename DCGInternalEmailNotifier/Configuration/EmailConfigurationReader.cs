﻿using System;
using System.Xml;

namespace DCGInternalEmailNotifier.Configuration
{
   public class EmailConfigurationReader
   {
       private DcgEmailConfiguration _dcgEmailConfiguration;
       
       public DcgEmailConfiguration DcgEmailConfiguration => _dcgEmailConfiguration;
       private XmlDocument _configurationXmlDocument;
        private readonly string _configurationFilePathAndName;

        public EmailConfigurationReader(string configurationFilePathAndName)
        {
            _configurationFilePathAndName = configurationFilePathAndName;
            LoadFile();
            SetConfiguration();
        }


        private void LoadFile()
        {
            try
            {
                _configurationXmlDocument = new XmlDocument();
                _configurationXmlDocument.Load($"{_configurationFilePathAndName}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private void SetConfiguration()
        {
            _dcgEmailConfiguration = new DcgEmailConfiguration();
            SetPort();
            SetDomain();
            SetHost();
            SetOrigin();
            SetEmailTargets();
            SetPassword();
            
        }


        private void SetPort()
        {
            _dcgEmailConfiguration.Port = Convert.ToInt32(_configurationXmlDocument.GetElementsByTagName("Port")[0].InnerText);


        }

        private void SetHost()
        {
            _dcgEmailConfiguration.Host = _configurationXmlDocument.GetElementsByTagName("Host")[0].InnerText;
        }


        private void SetOrigin()
        {
            _dcgEmailConfiguration.AddressOrigin = _configurationXmlDocument.GetElementsByTagName("OriginEmail")[0].InnerText;

        }


        private void SetDomain()
        {
            _dcgEmailConfiguration.Domain = _configurationXmlDocument.GetElementsByTagName("Domain")[0].InnerText;

        }

        private void SetPassword()
        {
            _dcgEmailConfiguration.AddressOriginPwd = _configurationXmlDocument.GetElementsByTagName("Password")[0].InnerText;

        }

        private void SetEmailTargets()
        {

            var emailElements = _configurationXmlDocument.GetElementsByTagName("Email");
            foreach (XmlElement emailElement in emailElements)
            {
                _dcgEmailConfiguration.AddEmailTarget(emailElement.InnerText);
            }


        }




    }
}
