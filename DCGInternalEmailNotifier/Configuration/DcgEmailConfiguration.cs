﻿using System.Collections.Generic;

namespace DCGInternalEmailNotifier.Configuration
{
    public class DcgEmailConfiguration
    {
        private List<string> _emailTargets;

        public DcgEmailConfiguration()
        {
            _emailTargets = new List<string>();
        }

        public string Host { get; set; }

        public string AddressOrigin { get; set; }
        public string AddressOriginPwd { get; set; }

        public int Port { get; set; }

        public string Domain { get; set; }

        public List<string> EmailTargets
        {
            get
            {
                if (_emailTargets == null) _emailTargets = new List<string>();
                return _emailTargets;
            }
        }

        public void AddEmailTarget(string emailAddress)
        {
            _emailTargets.Add(emailAddress);
        }
    }
}