﻿using DCGInternalEmailNotifier.Configuration;
using System.Net.Mail;

namespace DCGInternalEmailNotifier
{
    public class DcgEmailer
    {
        private DcgEmailConfiguration _emailConfiguration;

        public DcgEmailer(string configurationPath)
        { 
         SetConfiguration(configurationPath); 

        }

        private void SetConfiguration(string configPath)
        {
            EmailConfigurationReader reader = new EmailConfigurationReader(configPath);
            _emailConfiguration = reader.DcgEmailConfiguration;


        }

        public void SendEmail(string message,string subject)
        {
            MailMessage msg = new MailMessage();

            foreach (var emailTarget in _emailConfiguration.EmailTargets)
            {
                msg.To.Add(new MailAddress(emailTarget));


            }

            msg.From = new MailAddress(_emailConfiguration.AddressOrigin);

            msg.Subject = subject;
            msg.Body = message;
            msg.IsBodyHtml = true;
            SmtpClient client = new SmtpClient();
            client.Host = _emailConfiguration.Host;
            client.Credentials = new System.Net.NetworkCredential(_emailConfiguration.AddressOrigin, _emailConfiguration.AddressOriginPwd, _emailConfiguration.Domain);
            client.Port = _emailConfiguration.Port;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.EnableSsl = true;

            client.Send(msg);
        }


    }
}
